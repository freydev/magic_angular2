import {Component, View} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {Example2} from 'example-2';

@Component({
  selector: 'main'
})

@View({
  directives: [Example2],
  template: `
    <example-2></example-2>
  `
})

class Main {

}

bootstrap(Main);
