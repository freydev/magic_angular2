import {Component, View} from 'angular2/core';

@Component({
  selector: 'example-2'
})

@View({
  templateUrl: 'example-2.html'
})

export class Example2 {

  constructor() {
    console.info('Example2 Component Mounted Successfully');
  }

}
