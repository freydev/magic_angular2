import { Component, Input } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
  selector: '[radio]',
  templateUrl: `./radio.component.html`,
  styleUrls: ['./radio.component.styl']
})
 
export class RadioComponent extends DefaultUIComponent {
  @Input() id: string;
  @Input() name: string;
  @Input() label: string;
  @Input() value: string;
  @Input() required: any;
  @Input('checkedValue') checked: string;
 }