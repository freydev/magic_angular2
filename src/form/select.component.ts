import { Component, Input } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
    selector: '[select]',
    templateUrl: `./select.component.html`,
    styleUrls: ['./select.component.styl']
})

export class SelectComponent extends DefaultUIComponent {
    @Input() id: string;
    @Input() name: string;
    @Input() label: string;
    @Input() placeholder: string;
    @Input() model: any;
    @Input() required: any;
    @Input() items: string;
    @Input() value: any;
    @Input() selectModifiers: string;
    @Input() callback: Function;


    onChange(value: any){
        this.callback && this.callback(value)
    }
}