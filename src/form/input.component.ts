import { Component, Input, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
    selector: '[input]',
    templateUrl: `./input.component.html`,
    styleUrls: ['./input.component.styl'],
})

export class InputComponent extends DefaultUIComponent implements OnInit, OnChanges {
    @Input() id: string;
    @Input() name: string;
    @Input() type: string;
    @Input() label: string;
    @Input() placeholder: string;
    @Input() model: any;
    @Input() mask: any;
    @Input() required: any;
    @Input() value: string;
    @Input() error_tooltip: string;

    protected _mask: any = false;
    // protected model: any = '';

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    let log: string[] = [];
    for (let propName in changes) {
      let changedProp = changes[propName];
      let to = JSON.stringify(changedProp.currentValue);
      if (changedProp.isFirstChange()) {
        log.push(`Initial value of ${propName} set to ${to}`);
      } else {
        let from = JSON.stringify(changedProp.previousValue);
        log.push(`${propName} changed from ${from} to ${to}`);
      }
    }
    console.log(log.join(', '));
    }

    ngOnInit(){
        // переделать, не получилось прикнуть с шаблона
        if(this.mask){
            if(this.mask == 'phone')
                this._mask = [ /[7-8]/, '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/]
        }
        if(this.value) {
            this.model = this.value;
        }
    }


}