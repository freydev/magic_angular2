import { Component, Input } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
  selector: '[form]',
  templateUrl: `./form.component.html`,
  styleUrls: [`./form.component.styl`],
})
 
export class FormComponent extends DefaultUIComponent {
  @Input() background: string;
 }