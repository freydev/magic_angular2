import { FormComponent } from './form.component';
import { RadioComponent } from './radio.component';
import { InputComponent } from "./input.component";
import { SelectComponent } from "./select.component";

export const FORM_DIRECTIVES = [
  FormComponent,
  RadioComponent,
  SelectComponent,
  InputComponent
];