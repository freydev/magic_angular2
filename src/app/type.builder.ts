import {
    Component, ComponentFactory, NgModule, Input, Output, EventEmitter, Injectable, OnInit,
    Pipe, HostListener
} from '@angular/core';
import { JitCompiler } from '@angular/compiler';
import { PartsModule } from './parts.module';
import { UserService } from "../api/api.user.service";
import { QuizService } from "../api/api.quiz.service";
import { Observable } from "rxjs";
import { ApiService } from "../api/api.service";
import { Subscribable } from "rxjs/Observable";
import { GiftService } from "../api/api.gift.service";
import { TagService } from "../api/api.tag.service";
import { FormGroup } from "@angular/forms";

export interface IHaveDynamicData {
    entity: any;
}

@Pipe({
    name: 'phone'
})
export class PhonePipe {
    transform(value: string): any {
        if (!value) return;
        let match = value.match(/^7(\d{3})(\d{3})(\d{2})(\d{2})$/)
        return match && `+7 (${match[1]}) ${match[2]}-${match[3]}-${match[4]}` || value
    }
}

@Injectable()
export class DynamicTypeBuilder {

    constructor(protected compiler: JitCompiler) {
    }

    // this object is singleton
    private _cacheOfFactories: { [templateKey: string]: ComponentFactory<IHaveDynamicData> } = {};

    public createComponentFactory(template: string): Promise<ComponentFactory<IHaveDynamicData>> {
        let factory = this._cacheOfFactories[template];

        if (factory) {
            console.log("Module and Type are returned from cache")

            return new Promise((resolve) => {
                resolve(factory);
            });
        }

        let type = this.createNewComponent(template);
        let module = this.createComponentModule(type);

        return new Promise((resolve) => {
            this.compiler
                .compileModuleAndAllComponentsAsync(module)
                .then((moduleWithFactories) => {
                    factory = moduleWithFactories.componentFactories.find(f => f.componentType == type);

                    this._cacheOfFactories[template] = factory;

                    resolve(factory);
                });
        });
    }

    protected createNewComponent(tmpl: string) {
        @Component({
            selector: 'dynamic-component',
            template: tmpl,
        })
        class CustomDynamicComponent implements IHaveDynamicData {


            get showProfile(): any {
                return this._showProfile;
            }

            set showProfile(value: any) {
                if (value) {
                    document.body.style.overflow = "hidden";
                } else {
                    document.body.style.overflow = "";
                }
                this._showProfile = value;
            }

            get showHistory(): any {
                return this._showHistory;
            }

            set showHistory(value: any) {
                this.User.currentHistoryPage = 1;
                if (value) {
                    document.body.style.overflow = "hidden";
                } else {
                    document.body.style.overflow = "";
                }
                this._showHistory = value;
            }

            // TODO: сделать отедльную функцию для компонента modal для открытия

            @Input() public entity: any;

            private _showHistory: any = false;

            constructor(public Api: ApiService, public User: UserService, private Quiz: QuizService, public Gifts: GiftService, private Tag: TagService) {
                this.onResize();
            }

            // ------------------------ POLLS!!!!

            private _currentQuestion: number = 1;
            public quiz_complete: boolean;

            getCurrentQuestion(quiz: any[]) {
                return this._currentQuestion
            }

            nextQuestion(quiz: any[]) {
                if (quiz['result'].length) {
                    this._currentQuestion += 1;

                    if (this.getQuiz()['content'].questions.length == this._currentQuestion - 1)
                        this._currentQuestion = -1;

                    console.log(this._currentQuestion)
                }
            }

            completeQuiz(quiz: any[]) {
                let addTags = [];

                for (let res of quiz['content'].questions) {
                    for (let opt of res.result) {
                        console.log(opt)
                        if (opt.value.type == 'default')
                            addTags.push(opt.value.event)
                    }
                }

                this.Tag.add({tags: addTags})
                    .subscribe(value => {

                        this.Quiz.completeQuiz({
                            action_id: this.getQuiz()['id']
                        }).subscribe(value => {

                            this.quiz_complete = true;

                        })

                    })
            }

            setAnswer(quiz: any[], item: any[]) {
                if (quiz['answered']) return

                quiz['answered'] = true;
                quiz['result'].push(item);
            }

            getQuiz() {
                return this.Quiz.quizList
            }

            // ------------------------------------

            private _showProfile: any = false;


            getUser() {
                return this.User.currentUser['user']
            }

            getUserPoints() {
                return this.User.currentUser['user_points']
            }

            // FOR HISTORY

            getHistory() {
                return this.User.currentHistory
            }

            getDays() {
                return Array(31).fill(0).map((x, i) => {
                    return {value: i < 9 ? '0' + (i + 1) : i + 1, label: (i + 1)}
                });
            }

            getMonths(names: any[]) {
                return Array(names.length).fill(0).map((x, i) => {
                    return {value: i < 9 ? '0' + (i + 1) : i + 1, label: names[i]}
                });
            }

            getYears(startYear: number = 1960, currentYear: number = new Date().getFullYear()) {
                let years = new Array;
                while (startYear <= currentYear) years.push(startYear++);
                return years.map((x, i) => {
                    return {value: years[i], label: years[i]}
                });
            }

            // Stupid and fast solution
            getHistoryPage() {
                return this.User.currentHistoryPage
            }

            getPages(pageSize: number = this.giftPageSize, prefix: string) {
                return Array(Math.ceil(this.getHistory().length / pageSize)).fill(0).map((x, i) => {
                    return {value: i + 1, label: prefix + (i + 1)}
                });
            }

            goHistoryPage(page: number) {
                this.User.currentHistoryPage = page;
            }

            nextHistoryPage(pageSize: number = this.giftPageSize) {
                this.User.currentHistoryPage = (this.getHistoryPage() + 1) > Math.ceil(this.getHistory().length / pageSize) ? 1 : this.getHistoryPage() + 1;
            }

            prevHistoryPage(pageSize: number = this.giftPageSize) {
                this.User.currentHistoryPage = (this.getHistoryPage() - 1) * pageSize < 1 ? Math.ceil(this.getHistory().length / pageSize) : this.getHistoryPage() - 1;
            }

            // Maybe make some pipe??
            getHistoryName(historyItem: any, historyTexts: any) {
                return this.User.getHistoryName(historyItem, historyTexts)
            }

            getPurchaseForHistory(historyItem: any) {
                if (historyItem.purchaseData) {
                    delete historyItem.purchaseData;
                    return;
                }
                return this.User.getPurchase({id: historyItem.id}).subscribe((res: Response) => {
                    let response = res.json();
                    if (response && response['cart'] && response['cart']['cart'] && response['cart']['cart']['positions']) {
                        historyItem.purchaseData = response['cart']['cart']['positions'];
                    }
                })
            }

            // END FOR HISTORY


            // FOR GIFTS

            receiving: any = [];

            giftPageSize: number = 3;

            // Stupid and fast solution
            getGiftPage() {
                return this.Gifts.currentGiftPage
            }

            @HostListener('window:resize', ['$event'])
            onResize(event?: any) {
                let width = event ? event.target.innerWidth : window.innerWidth;
                if (width <= 400) {
                    this.giftPageSize = 1;
                } else if (width <= 650) {
                    this.giftPageSize = 2;
                } else {
                    this.giftPageSize = 3;
                }
            }

            nextGiftPage(pageSize: number = this.giftPageSize) {
                this.Gifts.currentGiftPage = (this.getGiftPage() + 1) > Math.ceil(this.getGifts().length / pageSize) ? 1 : this.getGiftPage() + 1;
            }

            prevGiftPage(pageSize: number = this.giftPageSize) {
                this.Gifts.currentGiftPage = (this.getGiftPage() - 1) * pageSize < 1 ? Math.ceil(this.getGifts().length / pageSize) : this.getGiftPage() - 1;
            }

            getGifts() {
                return this.Gifts.list
            }

            getGift(item: any) {

                this.Gifts.get(item).subscribe((final: Response) => {

                    // show notification
                    this.receiving.push(item.id);

                    // hide notification
                    setTimeout(() => {
                        let index = this.receiving.indexOf(item.id);
                        this.receiving.splice(index, 1);
                    }, 4000);

                    // update user info
                    this.User.getInfo().subscribe((response_user: Response) => {
                        if (response_user && response_user.json) {
                            let user = response_user.json();
                            this.User.currentUser = user;
                        }
                    });

                    // update history
                    this.User.getHistory()
                        .subscribe((response: Response) => {
                            if (response && response.json) this.User.currentHistory = response.json()['history']
                        });

                })
            }

            // END FOR GIFTS

            // Edit profile

            getUserPic(user: object, data: object = {
                           man: ['../assets/img/man1.svg', '../assets/img/man1.svg', '../assets/img/man3.svg'],
                           woman: ['../assets/img/woman1.svg', '../assets/img/woman2.svg', '../assets/img/woman3.svg'],
                           other: "../assets/img/other.svg"
                       }) {

                function _calculateAge(birthday: any) { // birthday is a date
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    return Math.abs(ageDate.getUTCFullYear() - 1970);
                }

                if (user && user['sex'] && user['birth_date'] && data) {

                    let age = _calculateAge(new Date(user['birth_date']));

                    switch (user['sex']) {

                        case 1: {
                            if (age <= 30) return data['man'][0];
                            if (age > 30 && age <= 50) return data['man'][1];
                            if (age > 50) return data['man'][2];
                            break;
                        }

                        case 2: {
                            if (age <= 30) return data['woman'][0];
                            if (age > 30 && age <= 50) return data['woman'][1];
                            if (age > 50) return data['woman'][2];
                            break;
                        }

                    }

                } else {
                    return data['other']
                }

            }

            // hack for fast
            editProfile(event: any) {
                let data = {};
                event.preventDefault();
                let inputs = event.target.parentNode.parentNode.getElementsByTagName('input');
                let selects = event.target.parentNode.parentNode.getElementsByTagName('select');
                let bday = [];
                for (let input of inputs) {

                    if (input.type != 'radio' || input.type == 'radio' && input.checked) {

                        if (input.name == 'addPhone' && input.value && input.value.replace(/\D/g, '') == this.getUser().phone ||
                            input.name == 'addEmail' && input.value == this.getUser().email ||
                            input.name == 'lastName' && input.value == this.getUser().last_name ||
                            input.name == 'firstName' && input.value == this.getUser().first_name
                        // TODO: переделать
                        // input.name == 'sex' && input.value == this.getUser().sex
                        ) {
                            continue;
                        } else {
                            // bred
                            if (input.name == 'addPhone') {
                                data[input.name] = input.value.replace(/\D/g, '')
                            } else {
                                data[input.name] = input.value
                            }

                        }

                    }
                }

                for (let select of selects) {

                    if (select.name == 'bd_d' && select.value != "undefined") {
                        bday[2] = select.value
                    }
                    if (select.name == 'bd_m' && select.value != "undefined") {
                        bday[1] = select.value
                    }
                    if (select.name == 'bd_y' && select.value != "undefined") {
                        bday[0] = select.value
                    }

                }


                if (bday[0] && bday[1] && bday[2] && bday.join('-') != this.getUser().birth_date) {
                    data['birthDate'] = bday.join('-');
                }

                this.User.update(data).subscribe((response: Response) => {
                    if (response && response.json) {
                        let res = response.json();
                        this.showProfile = false;
                        this.User.getInfo().subscribe((response_user: Response) => {
                            if (response_user && response_user.json) {
                                let user = response_user.json();
                                this.User.currentUser = user;
                            }
                        });
                    }
                })
            }

            // End edit profile

            logout() {
                return this.User.logout()
            }


        }

        return CustomDynamicComponent;
    }

    protected createComponentModule(componentType: any) {
        @NgModule({
            imports: [
                PartsModule,
            ],
            declarations: [
                PhonePipe,
                componentType
            ],
        })
        class RuntimeComponentModule {
        }

        return RuntimeComponentModule;
    }
}