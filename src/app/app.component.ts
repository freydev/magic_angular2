import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Response } from '@angular/http';

import { AppService } from './app.service';
import { ContainerComponent } from '../container/container.component'

require('../../assets/css/styles.styl')

import {UserService} from "../api/api.user.service";
import {QuizService} from "../api/api.quiz.service";
import {ApiService} from "../api/api.service";
import { GiftService } from "../api/api.gift.service";

@Component({
  selector: 'magic-app',
  template: '<dynamic-view></dynamic-view>',
  styleUrls: ['./app.component.styl'],
  encapsulation: ViewEncapsulation.Emulated
})

export class AppComponent implements OnInit {
  constructor(private service: AppService, private api:ApiService, private user: UserService, private quiz: QuizService, private gifts: GiftService) {}

  getCookie(name: string) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  ngOnInit() {
    // For testing
    let params = {
      // partner_id: 1717,
      partner_id: 151,
      domain: 'http://skazka.loc',
      lang: 'en',
      loyalty_config: 'default'
    };
    this.api.init(params).then(response => {
      let auth_hash = this.getCookie('sailplay_magic_auth_hash');
      if (auth_hash)
        this.user.login({auth_hash}).then(user=>{
          this.user.logged = true;
          this.user.getHistory()
              .subscribe((response: Response) => { if(response && response.json) this.user.currentHistory = response.json()['history'] });

          this.quiz.getQuiz()
            .subscribe((response: Response) => {
              if (response && response.json()) {
                this.quiz.quizList = response.json()['actions'][0]
              }
            });

          this.gifts.getList()
              .subscribe((response: Response) => { if(response && response.json) this.gifts.list = response.json()['gifts'] });

        });
    }, error => {
      console.error('An error occurred during the initialization: ', error)
    })

  }
 }