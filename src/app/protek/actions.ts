export default [{
    "block": "grid-item",
    "options": {
        "modifiers": "m-below900-span--20",
        "span": 4,
        "theming": {
            "font": {
                "family": "dinroundpro",
                "size": 46,
                "weight": 500
            }
        }
    },
    "data": [{
        "block": "text",
        "content": "Акции",
        "options": {
            "theming": {
                "margin": {
                    "top": 150
                },
                "color": "#fff"
            }
        }
    }, {
        "block": "text",
        "content": "Зарабатываете дополнительные<br>баллы выполняя задания",
        "options": {
            "theming": {
                "color": "#fff",
                "font": {
                    "size": 16
                },
                "margin": {
                    "top": 20,
                    "bottom": 20
                }
            }
        }
    },
    //     {
    //     "block": "button",
    //     "modifiers": "m-border--white m-shape--circle m-style--icon h-d--ib",
    //     "options": {
    //         "icon": "../assets/img/left-arrow-white05.svg",
    //         "hover_icon": "../assets/img/left-arrow-white.svg",
    //         "theming": {
    //             "size": {
    //                 "width": 50,
    //                 "height": 50
    //             },
    //             "margin": {
    //                 "right": 5
    //             }
    //         }
    //     }
    // }, {
    //     "block": "button",
    //     "modifiers": "m-border--white m-shape--circle m-style--icon h-d--ib",
    //     "options": {
    //         "icon": "../assets/img/right-arrow-white05.svg",
    //         "hover_icon": "../assets/img/right-arrow-white.svg",
    //         "theming": {
    //             "size": {
    //                 "width": 50,
    //                 "height": 50
    //             }
    //         }
    //     }
    // },
        {
        "block": "image",
        "options": {
            "src": "../assets/img/small-zigzag.svg",
            "theming": {
                "size": {
                    "height": 9,
                    "maxWidth": 154
                },
                "margin": {
                    "top": 38
                }
            }
        }
    }]
}, {
    "block": "grid-item",
    "options": {
        "modifiers": "h-mt--121 m-below900-mt--16 m-below900-span--20 m-below900-shift--0",
        "span": 15,
        "shift": 1,
        "theming": {}
    },
    "data": [{
        "block": "container",
        "options": {
            "bg-modifiers": "m-shape--round m-bgposition--right m-below650-bg--cover",
            "background": "#fff",
            "*ngFor": "let item of [1,2,3]",
            "bg-image": "../assets/img/action_type1.svg",
            "theming": {
                "margin": {
                    "bottom": 32
                },
                "size": {
                    "minHeight": 285
                }
            }
        },
        "data": [{
            "block": "grid",
            "options": {
                "theming": {
                    "size": {
                        "minHeight": 285
                    }
                }
            },
            "data": [{
                "block": "grid-item",
                "options": {
                    "span": 7,
                    "theming": {
                        "size": {
                            "height": 285
                        }
                    }
                },
                "data": [{
                    "block": "image",
                    "modifiers": "h-center h-w--80 h-center--abs",
                    "options": {
                        "src": "../assets/img/complivit.jpg",
                        "type": "image"
                    }
                }]
            }, {
                "block": "grid-item",
                "options": {
                    "span": 12,
                    "shift": 1
                },
                "data": [{
                    "block": "text",
                    "content": "Скидки 10% на все",
                    "options": {
                        "theming": {
                            "margin": {
                                "top": 50
                            },
                            "color": "blue",
                            "font": {
                                "size": 30,
                                "family": "dinroundpro",
                                "weight": 600
                            }
                        }
                    }
                }, {
                    "block": "text",
                    "content": "Специальная скидка 10% на лекарства<br>до 25 июля включительно",
                    "options": {
                        "theming": {
                            "margin": {
                                "top": 20
                            },
                            "font": {
                                "size": 16,
                                "family": "Roboto",
                                "weight": 400
                            },
                        }
                    }
                }]
            }]
        }],
    }]
}]