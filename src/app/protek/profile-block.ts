export default [{
    "block": "modal",
    "options": {
        "style.display": "!showProfile ? 'block' : 'none'"
    },
    "data": [{
        "block": "button",
        "modifiers": "m-style--icon m-rotate-onhover--45 h-transition-ease-out--300",
        "options": {
            "icon": "../assets/img/close-icon.svg",
            "(click)": "$event.preventDefault();showProfile = false",
            "theming": {
                "z-index": 1,
                "cursor": "pointer",
                "size": {
                    "width": 29,
                    "height": 29
                },
                "position": {
                    "right": 42,
                    "top": 42,
                    "type": "absolute"
                }
            }
        }
    }, {
        "block": "container",
        "options": {
            "modifiers": "m-width--auto",
            "bg-modifiers": "h-center--abs h-below750-center--disable h-overflow--auto",
            "theming": {
                "font": {
                    "family": "Roboto"
                }
            }
        },
        "data": [{
            "block": "container",
            "options": {
                "modifiers": "m-width--400",
                "bg-modifiers": "m-below750-mt--24 m-below900-span--14 m-below900-shift--3 m-below750-span--18 m-below750-shift--1",
                "theming": {
                    "font": {
                        "family": "dinroundpro",
                        "weight": 500
                    },
                }
            },
            "data": [{
                "block": "text",
                "content": "Профиль",
                "options": {
                    "align": "left",
                    "theming": {
                        "padding": {
                            "bottom": 30
                        },
                        "font": {
                            "size": 52,
                            "height": 56,
                        }
                    }
                },
            }, {
                "block": "form",
                "options": {},
                "data": [{
                    "block": "input",
                    "modifiers": "m-size--big m-shape--rounded m-theme--big-grey h-w--100 h-mb--24 h-bs--border-box",
                    "options": {
                        "id": "firstName",
                        "name": "firstName",
                        "tooltip_class": "to-left",
                        "description": "Это ваше имя",
                        "placeholder": "Имя",
                        "required": true,
                        "model": "!getUser()?.first_name",
                        "type": "text"
                    }
                }, {
                    "block": "input",
                    "modifiers": "m-size--big m-shape--rounded m-theme--big-grey h-w--100 h-mb--24 h-bs--border-box",
                    "options": {
                        "id": "lastName",
                        "name": "lastName",
                        "placeholder": "Фамилия",
                        "required": true,
                        "model": "!getUser()?.last_name",
                        "type": "text"
                    }
                }, {
                    "block": "input",
                    "modifiers": "m-size--big m-shape--rounded m-theme--big-grey h-w--100 h-mb--24 h-bs--border-box",
                    "options": {
                        "id": "addPhone",
                        "name": "addPhone",
                        "mask": "phone",
                        "error_tooltip": "true",
                        "required": true,
                        // "value": "!getUser()?.phone",
                        "model": "!getUser()?.phone",
                        "placeholder": "7 (___) ___ - __ - __",
                        "type": "text"
                    }
                }, {
                    "block": "input",
                    "modifiers": "m-size--big m-shape--rounded m-theme--big-grey h-w--100 h-mb--24 h-bs--border-box",
                    "options": {
                        "id": "addEmail",
                        "name": "addEmail",
                        "error_tooltip": "true",
                        "required": true,
                        "model": "!getUser()?.email",
                        "placeholder": "Email",
                        "type": "text"
                    }
                }, {
                    "block": "container",
                    "modifiers": "h-mb--24",
                    "data": [{
                        "block": "text",
                        "modifiers": "h-w--100 h-mb--16",
                        "content": "Дата рождения"
                    }, {
                        "block": "select",
                        "modifiers": "h-w--30 m-shape--rounded m-theme--big-grey h-bs--border-box h-below500-w--100",
                        "options": {
                            "selectModifiers": "h-below500-radius--25",
                            "items": "!getDays()",
                            "name": "bd_d",
                            "required": true,
                            "placeholder": "День",
                            "value": "!getUser()?.birth_date?.split('-')[2]",
                            "id": "bd_d",
                            "theming": {
                                "background": {
                                    "image": "url(../assets/img/select-arrow.svg)",
                                    "repeat": "no-repeat",
                                    "position": "88% center",
                                    "size": "auto 8px"
                                },
                                "padding": {
                                    "right": 40
                                },
                                "borderRadius": {
                                    "topRight": 0,
                                    "bottomRight": 0,
                                }
                            }
                        }
                    }, {
                        "block": "select",
                        "modifiers": "h-w--40 h-pl--4 h-pr--4 m-theme--big-grey h-bs--border-box h-text--center h-below500-w--100 h-below500-pl--0 h-below500-pr--0 h-below500-mt--10 h-below500-mb--10",
                        "options": {
                            "selectModifiers": "h-below500-radius--25",
                            "items": "!getMonths(['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'])",
                            "name": "bd_m",
                            "placeholder": "Месяц",
                            "id": "bd_m",
                            "required": true,
                            "value": "!getUser()?.birth_date?.split('-')[1]",
                            "theming": {
                                "background": {
                                    "image": "url(../assets/img/select-arrow.svg)",
                                    "repeat": "no-repeat",
                                    "position": "88% center",
                                    "size": "auto 8px"
                                },
                                "padding": {
                                    "right": 40
                                },
                                "borderRadius": {
                                    "topRight": 0,
                                    "bottomRight": 0,
                                    "topLeft": 0,
                                    "bottomLeft": 0,
                                }
                            }
                        }
                    }, {
                        "block": "select",
                        "modifiers": "h-w--30 m-shape--rounded m-theme--big-grey h-bs--border-box h-below500-w--100",
                        "options": {
                            "selectModifiers": "h-below500-radius--25",
                            "items": "!getYears()",
                            "name": "bd_y",
                            "placeholder": "Год",
                            "required": true,
                            "value": "!getUser()?.birth_date?.split('-')[0]",
                            "id": "bd_y",
                            "theming": {
                                "background": {
                                    "image": "url(../assets/img/select-arrow.svg)",
                                    "repeat": "no-repeat",
                                    "position": "88% center",
                                    "size": "auto 8px"
                                },
                                "padding": {
                                    "right": 40
                                },
                                "borderRadius": {
                                    "topLeft": 0,
                                    "bottomLeft": 0,
                                }
                            }
                        }
                    }]
                }, {
                    "block": "container",
                    "modifiers": "h-pb--24  h-pt--16",
                    "data": [{
                        "block": "text",
                        "modifiers": "h-d--ib h-below500-w--100  h-below500-mb--16",
                        "content": "Ваш пол"
                    }, {
                        "block": "radio",
                        "modifiers": "m-theme--small-blue h-d--ib h-ml--48 h-below500-ml--16",
                        "options": {
                            "id": "!'sex' + index",
                            "required": true,
                            "label": "!item.label",
                            "checkedValue": "!getUser()?.sex == item.value ? 'checked' : ''",
                            "[value]": "!item.value",
                            "name": "sex",
                            "*ngFor": "let item of [{label: 'Мужской', value: 1}, {label: 'Женский', value: 2}]; let index = index"
                        }
                    }]
                }, {
                    "block": "button",
                    "modifiers": "m-size--big m-shape--rounded m-color--blue-shadow h-text--center h-w--100 h-bs--border-box h-mb--24 h-mt--24",
                    "content": "Сохранить",
                    "options": {
                        "(click)": "editProfile($event)",
                        "theming": {
                            "font": {
                                "size": 16
                            },
                            "padding": {
                                "top": 18,
                                "bottom": 18
                            }
                        }
                    }
                }]
            }]
        }]
    }]
}, {
    "block": "modal",
    "options": {
        "style.display": "!showHistory ? 'block' : 'none'"
    },
    "data": [{
        "block": "button",
        "modifiers": "m-style--icon m-rotate-onhover--45 h-transition-ease-out--300",
        "options": {
            "icon": "../assets/img/close-icon.svg",
            "(click)": "$event.preventDefault();showHistory = false",
            "theming": {
                "z-index": 1,
                "cursor": "pointer",
                "size": {
                    "width": 29,
                    "height": 29
                },
                "position": {
                    "right": 42,
                    "top": 42,
                    "type": "absolute"
                }
            }
        }
    }, {
        "block": "container",
        "options": {
            "bg-modifiers": "h-center--abs h-below750-center--disable h-overflow--auto",
        },
        "data": [{
            "block": "grid",
            "data": [{
                "block": "grid-item",
                "options": {
                    "modifiers": "m-below750-mt--24 m-below750-span--18 m-below750-shift--1",
                    "span": 14,
                    "shift": 3,
                    "theming": {
                        "font": {
                            "family": "dinroundpro",
                            "weight": 500
                        },
                    }
                },
                "data": [{
                    "block": "text",
                    "content": "История",
                    "options": {
                        "align": "left",
                        "theming": {
                            "padding": {
                                "bottom": 30
                            },
                            "font": {
                                "size": 52,
                                "height": 56,
                            }
                        }
                    },
                }, {
                    "block": "container",
                    "click": "",
                    "options": {
                        "*ngFor": "let item of getHistory();let currentItem = index;",
                    },
                    "data": [{
                        "block": "container",
                        "modifiers": "m-background--pattern h-pt--24 h-pb--24",
                        "options": {
                            "*ngIf": "currentItem < (getHistoryPage() * 5) && currentItem >= (getHistoryPage() * 5 - 5)",
                            "bg-image": "../assets/img/bottom-zigzag.svg",
                            "bg-size": "auto 4px",
                            "theming": {
                                "background": {
                                    "position": "left bottom",
                                    "repeat": "repeat-x",
                                }
                            }
                        },
                        "data": [{
                            "block": "container",
                            "options": {
                                "bg-modifiers": "h-float--left h-w--30 h-below500-w--100"
                            },
                            "data": [{
                                "block": "text",
                                "content": "{{ getHistoryName(item,  {\"purchase\": \"Покупка\",\"share_badge\": \"Поделился наградой \",\"custom_action\": \"Выполнил\",\"earn_badge\": \"Получил награду \",\"gift_purchase\": \"Подарок\",\"promocode\": \"Активировал промокод\",\"registration\": \"Регистрация\",\"share_purchase\": \"Поедился покупкой в \",\"referred_purchase\": \"Друг произвел покупку\",\"social_share\": \"Рассказал о нас в \",\"referral\": \"Пригласил друга\",\"badge\": \"Награда\",\"enter_group\": \"Вступил в группу в \",\"referred\": \"Регистрация по инвайте\"})|| 'Не указано' }}",
                                "options": {
                                    "theming": {
                                        "font": {
                                            "size": 22,
                                            "height": 26,
                                        }
                                    }
                                }
                            }, {
                                "block": "text",
                                "content": "{{ item.action_date | date: 'dd.MM.y HH:mm' }}",
                                "options": {
                                    "theming": {
                                        "color": "#7D7D7D",
                                        "font": {
                                            "family": "Roboto",
                                            "size": 12,
                                            "height": 18,
                                        }
                                    }
                                }
                            }]
                        }, {
                            "block": "container",
                            "options": {
                                "bg-modifiers": "h-float--left h-w--40  h-below500-w--50",
                                "theming": {
                                    "size": {
                                        "minHeight": 1
                                    }
                                }
                            },
                            "data": [{
                                "block": "button",
                                "content": "Товары",
                                "modifiers": "m-size--default m-color--blue-blend m-shape--rounded m-style--texticon",
                                "options": {
                                    "(click)": "getPurchaseForHistory(item)",
                                    "*ngIf": "item.action == 'purchase'",
                                    "icon-modifiers": "!(item.purchaseData ? 'h-rotate---90' : 'h-rotate--90') + ' h-transition-ease-out--300'",
                                    "icon": "../assets/img/arrow.svg",
                                    "theming": {}
                                }
                            }, {
                                "block": "text",
                                "modifiers": "h-mt--16",
                                "options": {
                                    "*ngFor": "let info of item.purchaseData",
                                    "theming": {
                                        "color": "#000",
                                        "font": {
                                            "size": 16,
                                            "height": 26,
                                            "weight": 400
                                        }
                                    }
                                },
                                "content": "{{ info.product.name || info.product.sku || 'Не указано' }}"
                            }]
                        }, {
                            "block": "container",
                            "options": {
                                "bg-modifiers": "h-float--left h-w--30  h-below500-w--50"
                            },
                            "data": [{
                                "block": "text",
                                "content": "{{ item.points_delta }} бонусов",
                                "options": {
                                    "*ngIf": "item.points_delta >= 0",
                                    "align": "right",
                                    "theming": {
                                        "color": "#0752FF",
                                        "font": {
                                            "size": 22,
                                            "height": 26,
                                        }
                                    }
                                }
                            }, {
                                "block": "text",
                                "content": "{{ item.points_delta }} бонусов",
                                "options": {
                                    "*ngIf": "item.points_delta < 0",
                                    "align": "right",
                                    "theming": {
                                        "color": "#7D7D7D",
                                        "font": {
                                            "size": 22,
                                            "height": 26,
                                        }
                                    }
                                }
                            }, {
                                "block": "text",
                                "modifiers": "h-mt--16",
                                "options": {
                                    "align": "right",
                                    "*ngFor": "let info of item.purchaseData",
                                    "theming": {
                                        "color": "#000",
                                        "font": {
                                            "size": 16,
                                            "height": 26,
                                            "weight": 400
                                        }
                                    }
                                },
                                "content": "{{ (info.price | number) + ' руб.' || 'Не указано' }}"
                            }]
                        }]
                    }]
                }, {
                    "block": "container",
                    "modifiers": "h-d--ib h-pt--24 h-pb--24 h-below500-text--c",
                    "options": {},
                    "data": [{
                        "block": "select",
                        "modifiers": "m-shape--rounded m-theme--small-grey h-bs--border-box h-below500-w--100 h-below500-mb--16",
                        "options": {
                            "*ngIf": "getHistory()?.length > 5",
                            "items": "!getPages(5, 'Страница ')",
                            "name": "history_page",
                            "id": "history_page",
                            "[callback]": "goHistoryPage.bind(this)",
                            "value": "!getHistoryPage()",
                            "theming": {
                                "background": {
                                    "image": "url(../assets/img/select-arrow.svg)",
                                    "repeat": "no-repeat",
                                    "position": "88% center",
                                    "size": "auto 8px"
                                },
                                "padding": {
                                    "right": 40
                                }
                            }
                        }
                    }, {
                        "block": "button",
                        "modifiers": "m-border--blue m-shape--circle m-style--icon h-d--ib h-mr--10 h-ml--24 h-below500-ml--0",
                        "options": {
                            "*ngIf": "getHistory()?.length > 5",
                            "(click)": "prevHistoryPage(5)",
                            "icon": "../assets/img/left-arrow.svg",
                            "hover_icon": "../assets/img/left-arrow-blue.svg",
                            "style.vertical-align": "middle",
                            "style.display": "inline-block",
                            "theming": {
                                "size": {
                                    "width": 50,
                                    "height": 50
                                }
                            }
                        }
                    }, {
                        "block": "button",
                        "modifiers": "m-border--blue m-shape--circle m-style--icon h-d--ib",
                        "options": {
                            "*ngIf": "getHistory()?.length > 5",
                            "(click)": "nextHistoryPage(5)",
                            "icon": "../assets/img/right-arrow.svg",
                            "style.vertical-align": "middle",
                            "style.display": "inline-block",
                            "hover_icon": "../assets/img/right-arrow-blue.svg",
                            "theming": {
                                "size": {
                                    "width": 50,
                                    "height": 50
                                }
                            }
                        }
                    }]
                }]
            }]
        }]
    }]
}, {
    "block": "grid-item",
    "options": {
        "modifiers": "m-below1000-span--9 m-below850-span--20 h-mt--121 m-below750-mt--16",
        "span": 12,
        "theming": {
            "color": "white"
        },
    },
    "data": [{
        "block": "text",
        "content": "Защита здоровья<br>доступна для<br>каждого",
        "options": {
            "theming": {
                "font": {
                    "size": 52,
                    "height": 56,
                    "family": "dinroundpro",
                    "weight": 500
                }
            }
        }
    }, {
        "block": "text",
        "content": "Участник программ Мое здоровье получает гарантированную скидку<br>от 3% до 70% на более чем 16 000 лекарственных препаратов и товаров для<br>здоровья, которые можно удобно<br>и быстро получить в ближайшей аптеке.",
        "options": {
            "theming": {
                "font": {
                    "family": "Roboto",
                    "size": 14,
                    "height": 24
                },
                "margin": {
                    "top": 35
                }
            }
        }
    }, {
        "block": "button",
        "content": "Узнать больше",
        "modifiers": "m-size--default m-color--blue-blend m-shape--rounded m-style--texticon",
        "options": {
            "icon": "../assets/img/arrow.svg",
            "theming": {
                "margin": {
                    "top": 33
                }
            }
        }
    }]
}, {
    "block": "grid-item",
    "options": {
        "modifiers": "m-zindex--100 m-below1000-span--10 m-below900-span--12 m-below900-shift--4 m-below750-span--14 m-below750-shift--3 m-below600-span--20 m-below600-shift--0",
        "span": 8,
        "theming": {
            "margin": {
                "bottom": 100
            }
        }
    },
    "data": [{
        "block": "image",
        "modifiers": "h-center h-br--circle",
        "options": {
            // "src": "!getUser()?.avatar['250x250']",
            // "src": "../assets/img/man-10.svg",
            "src": "!getUser() && getUserPic(getUser())",
            "theming": {
                "size": {
                    "height": 150,
                    "width": 150
                },
                "position": {
                    "top": 69
                }
            }
        }
    }, {
        "block": "container",
        "options": {
            "bg-modifiers": "m-shape--shield m-overflow--hidden h-shadow--white h-on-hover--wrapper",
            "background": "white",
            "theming": {
                "margin": {
                    "top": -16
                }
            }
        },
        "data": [{
            "block": "button",
            "modifiers": "m-color--blue m-shape--circle m-style--icon",
            "plugins": [{
                "name": "tooltip",
                "theme": "default",
                "content": "Профиль",
            }],
            "options": {
                "(click)": "$event.preventDefault();$event.stopPropagation();showProfile = true",
                "icon": "../assets/img/open_profile.svg",
                "description": "Профиль",
                "theming": {
                    "size": {
                        "width": 50,
                        "height": 50
                    },
                    "position": {
                        "left": 55,
                        "top": 40,
                        "type": "absolute"
                    }
                }
            }
        }, {
            "block": "button",
            "modifiers": "m-color--blue m-shape--circle m-style--icon",
            "options": {
                "(click)": "$event.preventDefault();$event.stopPropagation();logout()",
                "icon": "../assets/img/logout.svg",
                "description": "Выход",
                "theming": {
                    "size": {
                        "width": 50,
                        "height": 50
                    },
                    "position": {
                        "right": 55,
                        "top": 40,
                        "type": "absolute"
                    }
                }
            }
        }, {
            "block": "text",
            "options": {
                "align": "center",
                "theming": {
                    "padding": {
                        "top": 107
                    },
                    "font": {
                        "size": 32,
                        "family": "dinroundpro",
                        "weight": 600
                    }
                }
            },
            "content": "{{ getUser()?.first_name ? getUser()?.first_name + ',' : '' }}<br> Здравствуйте"
        }, {
            "block": "text",
            "namespaces": "User.currentUser",
            "options": {
                "align": "center",
                "theming": {
                    "margin": {
                        "top": 18
                    },
                    "font": {
                        "family": "Roboto",
                        "height": 30
                    },
                    "color": "#808080"
                }
            },
            "content": "<span *ngIf=\"getUser()?.email\">{{ getUser()?.email }}<br></span>{{ getUser()?.phone | phone }}"
        }, {
            "block": "container",
            "data": [{
                "block": "button",
                "modifiers": "m-size--big m-shape--rounded m-color--blue-shadow h-center h-on-hover--visibility h-transition-ease-out--200--important",
                "content": "История",
                "options": {
                    "(click)": "$event.preventDefault();showHistory = true",
                    "theming": {
                        "size": {
                            "width": 55,
                        },
                        "margin": {
                            "left": "auto",
                            "right": "auto",
                        },
                        "position": {
                            "left": 0,
                            "right": 0,
                            "top": 0,
                            "type": "absolute"
                        }
                    }
                }
            }, {
                "block": "image",
                "options": {
                    "src": "../assets/img/rounded_holder.svg",
                    "theming": {
                        "margin": {
                            "top": 25
                        },
                        "size": {
                            "minHeight": 135
                        }
                    }
                },
                "data": [{
                    "block": "text",
                    "content": "{{ (getUserPoints()?.confirmed || 0) | number }} баллов",
                    "options": {
                        "align": "center",
                        "theming": {
                            "padding": {
                                "top": 26
                            },
                            "font": {
                                "size": 30,
                                "family": "dinroundpro",
                                "weight": 600
                            },
                            "color": "#0752FF"
                        }
                    }
                }, {
                    "block": "text",
                    "content": "+{{ (getUserPoints()?.unconfirmed || 0) | number }} неподвержденных",
                    "options": {
                        "description": "Ваши неподтвержденные баллы,<br>которые станут подтверждены<br>после факта оплаты заказа",
                        "tooltip_class": "to-top",
                        "align": "center",
                        "theming": {
                            "font": {
                                "size": 16,
                                "family": "dinroundpro",
                                "height": 24,
                                "weight": 500
                            },
                            "color": "#808080"
                        }
                    }
                }]
            }]
        }]
    }]
}]