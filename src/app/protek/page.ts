export const config = [{
	"block": "container",
	"modifiers": "h-p--24 m-background--pattern",
	"options": {
		"bg-modifiers": "m-overflow--hidden",
		"background": "#0752FF",
		"bg-image": "../assets/img/pattern-13.svg",
		"bg-size": "300px 288px",
	},
	"data": [{
		"block": "grid",
		"data": require('./profile-block').default
	}, {
		"block": "image",
		"modifiers": "m-expand m-half--ellipse",
		"options": {
			"type": "image",
			"src": "../assets/img/round-line.svg",
			"theming": {
				"position": {
					"bottom": -1,
					"type": "absolute"
				}
			}
		}
	}]
}, {
	"block": "container",
	"modifiers": "h-p--24",
	"options": {
		"background": "#fff",
		"theming": {
			"padding": {
				"top": 80
			}
		}
	},
	"data": [{
		"block": "image",
		"options": {
			"src": "../assets/img/star.svg",
			"theming": {
				"position": {
					"left": -40,
					"top": 11,
					"type": "absolute"
				},
				"size": {
					"height": 100,
					"width": 100
				}
			}
		}
	}, {
		"block": "image",
		"options": {
			"src": "../assets/img/star.svg",
			"theming": {
				"position": {
					"left": 64,
					"top": 440,
					"type": "absolute"
				},
				"size": {
					"height": 38,
					"width": 38
				}
			}
		}
	}, {
		"block": "image",
		"options": {
			"src": "../assets/img/star.svg",
			"theming": {
				"position": {
					"right": 20,
					"top": 20,
					"type": "absolute"
				},
				"size": {
					"height": 50,
					"width": 50
				}
			}
		},
	}, {
		"block": "grid",
		"data": require('./gifts-block').default,
		"options": {
			"theming": {
				"margin": {
					"bottom": 80
				}
			}
		}
	}, {
		"block": "image",
		"modifiers": "m-expand",
		"options": {
			"theming": {
				"position": {
					"type": "absolute",
					"bottom": -1
				}
			},
			"type": "image",
			"src": "../assets/img/zigzag.svg"
		}
	}],
}, {
	"block": "container",
	"modifiers": "h-p--24",
	"options": {
		"background": "#0752ff",
	},
	"data": [{
		"block": "image",
		"options": {
			"src": "../assets/img/star.svg",
			"style.opacity": ".15",
			"theming": {
				"position": {
					"left": -60,
					"top": 80,
					"type": "absolute"
				},
				"size": {
					"height": 80,
					"width": 80
				}
			}
		}
	}, {
		"block": "image",
		"options": {
			"src": "../assets/img/star.svg",
			"style.opacity": ".15",
			"theming": {
				"position": {
					"left": -64,
					"top": 680,
					"type": "absolute"
				},
				"size": {
					"height": 48,
					"width": 48
				}
			}
		}
	}, {
		"block": "image",
		"options": {
			"src": "../assets/img/star.svg",
			"style.opacity": ".15",
			"theming": {
				"position": {
					"left": 20,
					"top": 777,
					"type": "absolute"
				},
				"size": {
					"height": 80,
					"width": 80
				}
			}
		},
	}, {
		"block": "grid",
		"options": {
			"theming": {
				"margin": {
					"bottom": 80
				}
			}
		},
		"data": require('./actions.ts').default
	}]
}, {
	"block": "container",
	"modifiers": "h-p--24",
	"options": {
		"background": "#FFF",
		"class.h-hidden": "!quiz_complete || !getQuiz()"
	},
	"data": [{
		"block": "image",
		"modifiers": "m-expand h-flip--vertical",
		"options": {
			"theming": {
				"position": {
					"type": "absolute",
					"top": -1
				}
			},
			"type": "image",
			"src": "./assets/img/zigzag.svg"
		}
	}, {
		"block": "grid",
		"data": [{
			"block": "grid-item",
			"options": {
				"modifiers": "m-below900-span--20",
				"span": 4
			},
			"data": [{
				"block": "text",
				"content": "Опрос<br>за баллы",
				"options": {
					"theming": {
						"font": {
							"family": "dinroundpro",
							"size": 46,
							"weight": 500
						},
						"margin": {
							"top": 150
						}
					}
				}
			}, {
				"block": "text",
				"content": "За прохождение<br>опроса Вы получите<br>дополнительные<br>баллы",
				"options": {
					"theming": {
						"color": "#0752FF",
						"font": {
							"size": 16,
							"family": "dinroundpro"
						},
						"margin": {
							"top": 20,
							"bottom": 20
						}
					}
				}
			}]
		}, {
			"block": "grid-item",
			"options": {
				"modifiers": "h-mt--150 m-below900-mt--16 m-below900-span--20 m-below900-shift--0",
				"span": 15,
				"shift": 1,
				"theming": {
					"font": {
						"family": "dinroundpro",
						"weight": 500,
						"size": 20
					}
				}
			},
			"data": [{
				"block": "container",
				"options": {
					"*ngFor": "let quiz of getQuiz()?.content.questions; let qi = index",
					"class.h-hidden": "!getCurrentQuestion() != qi + 1"					
				},
				"data": [{
					"block": "text",
					"content": "Вопрос {{ qi + 1 }}/{{ getQuiz()?.content.questions.length }}",
				}, {
					"block": "text",
					"options": {
						"theming": {
							"font": {
								"size": 25,
								"weight": 600
							},
							"margin": {
								"top": 5,
								"bottom": 50
							}
						}
					},
					"content": "{{ quiz.text }}"
				}, {
					"block": "form",
					"data": [{
						"block": "radio",
						"modifiers": "m-theme--big-blue",
						"options": {
							"id": "!'answer_' + qi + '_' + index",
							"label": "!item.text",
							"name": "!'answer_' + qi",
							"*ngFor": "let item of quiz.options; let index = index",
							"(click)": "setAnswer(quiz, item)"
						}
					}, {
						"block": "button",
						"modifiers": "m-size--big m-shape--rounded m-color--blue-shadow",
						"content": "Следующий вопрос",
						"options": {
							"(click)": "nextQuestion(quiz)",
							"theming": {
								"margin": {
									"top": 38,
									"bottom": 138
								}
							}
						}
					}]
				}]
			}, {
				"block": "container",
				"options": {
					"class.h-hidden": "!getCurrentQuestion() > -1"										
				},
				"data": [{
					"block": "text",
					"options": {
						"theming": {
							"font": {
								"size": 25,
								"weight": 600
							},
							"margin": {
								"top": 5,
								"bottom": 50
							}
						}
					},
					"content": "Поздравляем!<br>Вы прошли опрос и получаете за это 100 баллов.<br>Позже будут доступны новые опросы."
				}, {
					"block": "form",
					"data": [{
						"block": "button",
						"modifiers": "m-size--big m-shape--rounded m-color--blue-shadow",
						"content": "Завершить опрос",
						"options": {
							"(click)": "completeQuiz(getQuiz())",
							"theming": {
								"margin": {
									"top": 38,
									"bottom": 138
								}
							}
						}
					}]
				}]
			}]
		}]
	}]
}]