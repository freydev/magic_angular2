export default [{
    "block": "grid-item",
    "options": {
        "modifiers": "m-below900-span--20 h-mt--150 m-below750-mt--16",
        "span": 5,
        "theming": {
            "font": {
                "family": "dinroundpro",
                "size": 46,
                "weight": 500
            },
        }
    },
    "data": [{
        "block": "text",
        "content": "Подарки",
        "options": {
            "theming": {}
        }
    }, {
        "block": "text",
        "content": "Накопленные баллы<br>Вы можете обменять<br>на эти подарки",
        "options": {
            "theming": {
                "color": "#0752FF",
                "font": {
                    "size": 16
                },
                "margin": {
                    "top": 20,
                    "bottom": 20
                }
            }
        }
    }, {
        "block": "button",
        "modifiers": "m-border--blue m-shape--circle m-style--icon h-d--ib",
        "options": {
            "icon": "../assets/img/left-arrow.svg",
            "(click)": "$event.preventDefault();prevGiftPage(giftPageSize)",
            "hover_icon": "../assets/img/left-arrow-blue.svg",
            "*ngIf": "getGifts()?.length > giftPageSize",
            "theming": {
                "size": {
                    "width": 50,
                    "height": 50
                },
                "margin": {
                    "right": 5
                }
            }
        }
    }, {
        "block": "button",
        "modifiers": "m-border--blue m-shape--circle m-style--icon h-d--ib",
        "options": {
            "icon": "../assets/img/right-arrow.svg",
            "*ngIf": "getGifts()?.length > giftPageSize",
            "(click)": "$event.preventDefault();nextGiftPage(giftPageSize)",
            "hover_icon": "../assets/img/right-arrow-blue.svg",
            "theming": {
                "size": {
                    "width": 50,
                    "height": 50
                }
            }
        }
    }]
}, {
    "block": "grid-item",
    "options": {
        "modifiers": "m-below900-span--0 m-below900-col--6 m-below900-shift--1  m-below650-span--10 m-below650-shift--0 m-below400-span--20",
        "*ngFor": "let item of getGifts();let currentItem = index;",
        "span": 5,
        "[style.display]": "currentItem < (getGiftPage() * giftPageSize) && currentItem >= (getGiftPage() * giftPageSize - giftPageSize) ? 'block' : 'none'",
        "theming": {
            "font": {
                "family": "dinroundpro"
            }
        }
    },
    "data": [{
        "block": "container",
        "options": {
            "*ngIf": "currentItem < (getGiftPage() * giftPageSize) && currentItem >= (getGiftPage() * giftPageSize - giftPageSize)",
            "bg-modifiers": "m-shape--round m-hover--blend m-hover--round_img h-on-hover--wrapper h-text--center",
            "theming": {
                "size": {
                    "minHeight": 430
                }
            }
        },
        "data": [{
            "block": "image",
            "modifiers": "h-center",
            "options": {
                "src": "!item.thumbs.url_250x250",
                "type": "image",
                "theming": {
                    "margin": {
                        "top": 50,
                        "bottom": 41
                    },
                    "size": {
                        "width": 150,
                        "height": 150
                    }
                }
            }
        }, {
            "block": "text",
            "content": "{{ item.name  }}",
            "modifiers": "h-center",
            "options": {
                "theming": {
                    "font": {
                        "size": 20,
                        "weight": 500
                    },
                    "size": {
                        "maxWidth": 160
                    }
                }
            }
        }, {
            "block": "text",
            "content": "{{ item.points }} баллов",
            "modifiers": "h-center",
            "options": {
                "theming": {
                    "color": "#0752FF",
                    "font": {
                        "size": 25,
                        "weight": 600,
                        "height": 40
                    },
                    "size": {
                        "maxWidth": 160
                    }
                }
            }
        }, {
            "block": "text",
            "content": "{{ item.descr }}",
            "modifiers": "h-center",
            "options": {
                "*ngIf": "receiving.indexOf(item.id) == -1",
                "theming": {
                    "color": "#7D7D7D",
                    "font": {
                        "family": "Roboto",
                        "size": 12,
                        "height": 18
                    },
                    "size": {
                        "maxWidth": 160
                    }
                }
            }
        }, {
            "block": "text",
            "content": "Подарок успешно получен.",
            "modifiers": "h-center",
            "options": {
                "*ngIf": "receiving.indexOf(item.id) != -1",
                "theming": {
                    "color": "#0752FF",
                    "font": {
                        "family": "dinroundpro",
                        "size": 16,
                        "weight": 500,
                        "height": 20
                    },
                    "size": {
                        "maxWidth": 160
                    }
                }
            }
        }, {
            "block": "button",
            "modifiers": "m-size--big m-shape--rounded m-color--blue-shadow h-center h-on-hover--visibility h-transition-ease-out--200--important h-mt--16",
            "content": "Получить",
            "options": {
                "*ngIf": "receiving.indexOf(item.id) == -1 && item.points <= getUserPoints()?.confirmed",
                "(click)": "$event.preventDefault();getGift(item)"
            }
        }, {
            "block": "button",
            "modifiers": "m-size--big m-shape--rounded m-color--blue-blend h-center h-on-hover--visibility h-transition-ease-out--200--important h-mt--16",
            "content": "Получить",
            "options": {
                "*ngIf": "receiving.indexOf(item.id) == -1 && item.points > getUserPoints()?.confirmed",
                "(click)": "$event.preventDefault();"
            }
        }]
    }]
}]