import { NgModule, Directive, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// parts module
import { PartsModule } from './parts.module';

// detail stuff
import { DynamicView } from './dynamic.view';
import { DynamicTypeBuilder } from './type.builder';
import { DynamicTemplateBuilder } from './template.builder';

import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [ PartsModule, FormsModule],
  declarations: [ DynamicView ],
  exports: [ DynamicView],
})

export class DynamicModule {

    static forRoot()
    {
        return {
            ngModule: DynamicModule,
            providers: [
              DynamicTemplateBuilder,
              DynamicTypeBuilder
            ], 
        };
    }
}