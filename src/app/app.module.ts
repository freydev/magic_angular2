import { NgModule, Directive, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { COMPILER_PROVIDERS } from '@angular/compiler';

import { JsonpModule } from '@angular/http';
import { DynamicModule }    from './dynamic.module';

import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { UserService } from "../api/api.user.service";
import { ApiService } from "../api/api.service";
import { QuizService } from "../api/api.quiz.service";
import { GiftService } from "../api/api.gift.service";
import { TagService } from "../api/api.tag.service";

@NgModule({
    imports: [
        BrowserModule,
        JsonpModule,
        DynamicModule.forRoot()
    ],
    providers: [
        AppService,
        COMPILER_PROVIDERS,
        ApiService,
        UserService,
        QuizService,
        GiftService,
        TagService
    ],
    declarations: [
        AppComponent,
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}