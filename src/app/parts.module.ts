// make all parts as one DYNAMIC_DIRECTIVES 
import { forwardRef, Directive, Input } from '@angular/core';
import { ContainerComponent } from '../container/container.component';
import { TextComponent } from '../text/text.component';
import { ImageComponent } from '../image/image.component';
import { ButtonComponent } from '../button/button.component';
import { ModalComponent } from "../modal/modal.component";
import { GRID_DIRECTIVES } from '../grid';
import { FORM_DIRECTIVES } from '../form';
import { ToolTipModule } from '../plugins/tooltip/tooltip.module';
import { QuizHandler, QuizControls } from '../handlers/quiz.handler.component'

@Directive({
    selector: '[preventdefault]',
    host: {
        '(click)': 'OnClick($event)'
    }
})
class PreventDefault {
    OnClick(event: any) {
        event.preventDefault();
    }
}

export const DYNAMIC_DIRECTIVES = [...
    GRID_DIRECTIVES,
    FORM_DIRECTIVES,
    forwardRef(() => ImageComponent),
    forwardRef(() => ModalComponent),
    forwardRef(() => ContainerComponent),
    forwardRef(() => TextComponent),
    forwardRef(() => ButtonComponent),
    forwardRef(() => QuizControls),
    forwardRef(() => PreventDefault),
];

// module itself
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [
      TextMaskModule,
      FormsModule,
      CommonModule,
      ToolTipModule,
  ],
  declarations: [
      DYNAMIC_DIRECTIVES
  ],
  exports: [
      DYNAMIC_DIRECTIVES,
      CommonModule
  ]
})
export class PartsModule {

    static forRoot()
    {
        return {
            ngModule: PartsModule
        };
    }
}