import {Injectable} from "@angular/core";
import { config } from './protek/page'

@Injectable()
export class DynamicTemplateBuilder {

    public prepareTemplate(){
        return ((function doDFS(v: any[]) {
            let html = '<style>:host { color: inherit; }</style>';
            for (let b of v) {
                let tag = b.block;
                let attrs = [];

                if( b.modifiers ) attrs.push(`modifiers="${b.modifiers}"`);
                if( b.options ) {
                    for (let o of Object.keys(b.options)) {

                        let first_part = `[${o}]`,
                            second_part = `${JSON.stringify(b.options[o])}`;

                        if (~'*(#['.indexOf(o[0])) {
                            first_part = o;
                            second_part = `${b.options[o]}`
                        }

                        if (b.options[o][0] == '!') {
                            second_part = b.options[o].slice(1)
                        }

                        if (o == 'handlers') {
                            attrs.push(`${b.options[o]}`)
                            continue;
                        }

                        if (~'!'.indexOf(o[0])) {
                            first_part = o.slice(1);
                            second_part = b.options[o];
                            attrs.push(`${first_part}="${second_part}"`)
                            continue;
                        }

                        attrs.push(`${first_part}="${second_part.replace(/"/g, '\'')}"`)
                    }
                }  

                html += `<div ${tag} ${attrs.join(' ')}>`
                html += `${b.data && doDFS(b.data) || b.content || ''}`
                html += `</div>`

            }

            return html
        })(config))

    }
}