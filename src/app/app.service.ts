import { Injectable } from '@angular/core';
import { Jsonp } from '@angular/http';
 
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
 
@Injectable()
export class AppService {
  constructor(private jsonp: Jsonp) {}

  loadConfig(partner: number, name: string = 'default'): Promise<JSON> {
    return this.jsonp.get(`https://sailplay.net/js-api/${partner}/loyalty-page/config/by-name/?name=${name}&callback=JSONP_CALLBACK`)
      .toPromise().then(response => {
        return response.json().config as JSON
      })
      .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error)
  }
}