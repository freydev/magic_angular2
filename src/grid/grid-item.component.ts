import { Component, Input, HostBinding, OnInit } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
    selector: '[grid-item]',
    templateUrl: './grid-item.component.html',
    styleUrls: [`./grid-item.component.styl`],
})

export class GridItemComponent extends DefaultUIComponent implements OnInit {
    @Input() columns: number
    @Input() span: number
    @Input() shift: number
    
    @HostBinding('class') className = ''

    ngOnInit() {
        let classes: string[] = []
        let shift_class: string = 'shift-col--';

        if (this.span) {
            classes.push(`span--${this.span}`);
            shift_class = 'shift--'
        }

        if (this.columns) classes.push(`col--${this.columns}`);
        if (this.shift) classes.push(`${shift_class}${this.shift}`);
        if (this.modifiers) classes = classes.concat(this.modifiers.split(' '));

        this.className = classes.join(' ')
    }
}