import { Component } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
    selector: '[grid]',
    templateUrl: `./grid.component.html`,
    // styleUrls: [`./${name}component.styl`]
})

export class GridComponent extends DefaultUIComponent { }