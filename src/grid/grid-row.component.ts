import { Component } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
    selector: '[grid-row]',
    templateUrl: `./grid-row.component.html`,
    styleUrls: [`./grid-row.component.styl`]
})

export class GridRowComponent extends DefaultUIComponent { }