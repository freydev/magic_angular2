import { Component, Input } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

type ImageType = 'background' | 'image';

@Component({
  selector: '[image]',
  styleUrls: ['./image.component.styl'],
  templateUrl: './image.component.html'
})

export class ImageComponent extends DefaultUIComponent {
    @Input() private type: ImageType = 'background';
    @Input() private src: string;
}
