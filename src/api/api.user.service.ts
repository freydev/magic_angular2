import { Injectable, OnInit } from "@angular/core";
import {Jsonp} from '@angular/http';
import {ApiClass, ApiService} from './api.service';

interface IUser {
    user_status: object;
    user_points: {
        confirmed: number;
        unconfirmed: number;
    };
    user: {
        first_name: string;
        last_name: string;
        middle_name: string;
        name: string;
        pic: string;
        is_email_notifications: number;
        phone: string;
        register_date: string;
        avatar: object;
        is_sms_notifications: number;
        birth_date: object;
        sex: number;
        email: object;
        origin_user_id: object;
    };
    purchases: {
        count: number;
        sum: number
    };
    last_badge: object;
}

@Injectable()
export class UserService extends ApiClass {
    currentUser: object = {};
    currentHistory: object[] = [];
    currentHistoryPage: number = 1;
    logged: boolean = false;    

    constructor(protected jsonp: Jsonp, protected api: ApiService) {
        super();
    }

    getHistory(params: any = {}) {
        params['tz'] = this.api.getTimeZone();
        let url = this.getUrl('users.history');
        return this.request(url, params)
    }

    getHistoryName(historyItem: any, historyTexts: any){
        switch (historyItem.action) {
            case 'custom_action':
                return historyTexts.custom_action + ': ' + historyItem.name;
            case 'gift_purchase':
                return historyTexts.gift_purchase + ': ' + historyItem.name;
            case 'event':
                return historyItem.name || historyTexts.custom_action;
            case 'extra':
                return historyItem.name || historyTexts.custom_action;
            case 'sharing':
                switch (historyItem.social_action) {
                    case 'like':
                        return historyTexts.enter_group + historyItem.social_type;
                    case 'purchase':
                        return historyTexts.share_purchase + historyItem.social_type;
                    case 'partner_page':
                        return historyTexts.social_share + historyItem.social_type;
                    case 'badge':
                        return historyTexts.share_badge + historyItem.social_type;
                }
        }
        return historyTexts[historyItem.action];
    }

    getInfo(params: any = {}) {
        params['user_status'] = 1;
        params['badges'] = 1;
        params['last_badge'] = 1;
        params['all'] = 1;
        params['purchases'] = 1;
        let url = this.getUrl('users.info');
        return this.request(url, params)
    }

    getPurchase(params: any = {}) {
        let url = this.getUrl('purchases.get');
        return this.request(url, params)
    }

    update(params: any = {}) {
        let url = this.getUrl('users.update');
        return this.request(url, params)
    }

    login(params: any) {
        var self = this;
        return new Promise((resolve, reject):void => {
            self.getInfo(params)
                .toPromise()
                .then((response: any) => {
                    let user = response.json();
                    self.currentUser = user;
                    self.api['partner_config']['auth_hash'] = params.auth_hash;
                    localStorage.setItem('auth_hash', params.auth_hash);
                    resolve(user);
                }, reject);
        })

    }

    logout() {
        return new Promise((resolve, reject) => {
            let self = this;
            let domain = this.api['partner_config']['DOMAIN'];
            let req = document.createElement('iframe');
            req.width = '0';
            req.height = '0';
            req.style.border = 'none';
            req.src = domain + '/users/logout/';
            document.body.appendChild(req);
            req.onload = function () {
                document.body.removeChild(req);
                self.api['partner_config']['auth_hash'] = '';
                localStorage.removeItem('auth_hash');
                resolve();
            };
            req.onerror = function () {
                reject();
            }
        })
    }


}