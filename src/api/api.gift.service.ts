import { Injectable } from "@angular/core";
import { ApiClass, ApiService } from './api.service';
import { Observable } from "rxjs";
import { Jsonp } from "@angular/http";


@Injectable()
export class GiftService extends ApiClass {

    list: object[] = [];

    currentGiftPage: number = 1;

    constructor(protected jsonp: Jsonp, protected api: ApiService) {
        super();
    }

    info(params: any) {
        let request_params = {
            gift_id: params.id
        };
        let url = this.getUrl('gifts.get');
        return this.request(url, request_params)
    }

    getList() {
        let url = this.getUrl('gifts.list');
        return this.request(url)
    }

    categories(params: any) {
        // let url = this.getUrl('gifts.list');
        // return this.request(url, params)
    }

    // TODO: ЖЕСТЬ доделать метод
    get(params: any) {
        let request_params = {
            gift_id: params.id,
            dep_id: this.api['partner_config']['dep_id'] || this.api['partner_config']['partner']['depId'] || ''
        };
        let url = this.getUrl('gifts.purchase.purchase');
        return new Observable(observer => {

                this.request(url, request_params)
                    .subscribe((response_1: Response) => {
                        //  RESPONSE 1
                        let res = response_1.json();
                        if (res['status'] == 'ok' && res['is_completed']) {

                            if (res['is_completed']) {

                                if (!res['request_to_partner_url']) {

                                    let params = {
                                        gift_public_key: res['gift_public_key'],
                                        auth_hash: this.api['partner_config']['auth_hash']
                                    };

                                    if (request_params['options'] && request_params['options'].no_user_sms) params['no_user_sms'] = request_params['options'].no_user_sms;

                                    this.request(this.getUrl('gifts.purchase.force_confirm'), params)
                                        .map((response_3: Response) => response_3.json())
                                        //  RESPONSE 3
                                        .subscribe(success => {
                                            console.log('force_confirm success', success);
                                            observer.next();
                                        }, error => {
                                            console.log('force_confirm error', error);
                                        });


                                }

                            } else {

                                let reqGiftPurchase = {
                                    gift_public_key: res['gift_public_key'],
                                    gift_sku: res['gift_sku'],
                                    auth_hash: this.api['partner_config']['auth_hash']
                                };

                                if (res['user_phone']) reqGiftPurchase['user_phone'] = res['user_phone'];
                                if (res['email']) reqGiftPurchase['email'] = res['email'];

                                this.request(res['request_to_partner_url'])
                                    .map((response_2: Response) => response_2.json())
                                    .subscribe(success => {
                                        //  RESPONSE 2
                                        console.log('request_to_partner_url success', success);
                                    }, error => {
                                        console.log('request_to_partner_url error', error);

                                        let params = {
                                            gift_public_key: res['gift_public_key'],
                                            auth_hash: this.api['partner_config']['auth_hash']
                                        };

                                        if (request_params['options'] && request_params['options'].no_user_sms) params['no_user_sms'] = request_params['options'].no_user_sms;

                                        this.request(this.getUrl('gifts.purchase.force_confirm'), params)
                                            .map((response_4: Response) => response_4.json())
                                            .subscribe(success => {
                                                console.log('force_confirm success 2', success);
                                                observer.next();
                                            }, error => {
                                                console.log('force_confirm error 2', error);
                                            });

                                    });

                            }

                        }

                    })

            }
        );
    }


}