import {Injectable} from "@angular/core";
import {Jsonp} from '@angular/http';
import {ApiService, ApiClass} from './api.service';


@Injectable()
export class QuizService extends ApiClass {
    quizList: any[];

    constructor(protected jsonp: Jsonp, protected api: ApiService) { 
        super()
    }

    getQuiz() {
        let url = this.getUrl('actions.custom.list');
        return this.request(url, {})
    }

    completeQuiz(params: object) {
        let url = this.getUrl('actions.custom.complete')
        return this.request(url, params)
    }

}