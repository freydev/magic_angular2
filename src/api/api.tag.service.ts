import {Injectable} from "@angular/core";
import {Jsonp} from '@angular/http';
import {ApiClass, ApiService} from './api.service';


@Injectable()
export class TagService extends ApiClass {
    constructor(protected jsonp: Jsonp, protected api: ApiService) { 
        super()
    }
    /**
     * Add tags
     * @param {tags: Array}
     * @returns {Observable<Response>}
     */
    add(params: object) {
        if (params['tags']) {
            params['tags'] = params['tags'] && params['tags'].join(',') || [];
        }
        let url = this.getUrl('tags.add');
        return this.request(url, params)
    }

    /**
     * Check tags existing
     * @param {tags: Array}
     * @returns {Observable<Response>}
     */
    exist(params: object) {
        if (params['tags']) {
            params['tags'] = JSON.stringify(params['tags'] || []);
        }
        let url = this.getUrl('tags.exist');
        return this.request(url, params)
    }

    /**
     * Delete tags
     * @param {tags: Array}
     * @returns {Observable<Response>}
     */
    delete(params: object) {
        if (params['tags']) {
            params['tags'] = params['tags'] && params['tags'].join(',') || [];
        }
        let url = this.getUrl('tags.delete');
        return this.request(url, params)
    }

}