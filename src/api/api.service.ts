import { Injectable } from "@angular/core";
import { Jsonp, URLSearchParams, QueryEncoder } from '@angular/http';
import { Observable } from 'rxjs/Observable';

interface PartnerConfigParams {
    partner_id: number;
    domain: string;
    lang: string;
    loyalty_config: string;
    dep_id?: number;
}

// Helper for sending encoded plus symbol
export class CustomQueryEncoderHelper extends QueryEncoder {
    encodeKey(k: string): string {
        k = super.encodeKey(k);
        return k.replace(/\+/gi, '%2B');
    }
    encodeValue(v: string): string {
        v = super.encodeKey(v);
        return v.replace(/\+/gi, '%2B');
    }
}

export abstract class ApiClass {
    protected jsonp: Jsonp;
    protected api: ApiService;

    // TODO: переделать на '?'
    getUrl(path: string) {
        if (this.api.inited) {
            let _url = path.split('.').reduce((previous, current) => {
                return previous[current];
            }, this.api['partner_config']['urls']);
            return this.api['partner_config']['domain'] + _url + '?callback=JSONP_CALLBACK';
        } else {
            return '';
        }
    }

    request(url: string, params: object = {}) {
        // If api is not yet initialized
        if (!this.api.inited) return new Observable(observer => observer.complete());

        let request_params = new URLSearchParams('', new CustomQueryEncoderHelper());

        for (let i in params)
            request_params.set(i, params[i]);

        if (!params['auth_hash'])
            request_params.set('auth_hash', this.api['partner_config']['auth_hash']);

        if (!params['lang'])
            request_params.set('lang', this.api['partner_config']['lang']);

        // if(!params['dep_id'])
        //     request_params.set('lang', this.api['partner_config']['dep_id'] || this.api['partner_config']['partner']['depId'] || '');

        return this.jsonp.get(url, {params: request_params})
    }
}

@Injectable()
export class ApiService {
    partner_config: object;
    loyalty_config: object;
    inited = false;

    constructor(private jsonp: Jsonp) {
    }

    getTimeZone() {
        let offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
        return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
    }

    getPartnerConfig(config: PartnerConfigParams) {
        let url = `${config.domain}/js-api/${config.partner_id}/config/?callback=JSONP_CALLBACK`;
        let request_params = {
            params: {
                lang: config.lang
            }
        };
        return this.jsonp.get(url, request_params)
    }

    getLoyaltyConfig(name: string = 'default', partner_config: object = this.partner_config) {
        let url = partner_config['domain'] + partner_config['urls']['loyalty_page_config_by_name'] + '?callback=JSONP_CALLBACK';
        let request_params = {
            params: {
                name: name
            }
        };
        return this.jsonp.get(url, request_params)
    }

    init(config: PartnerConfigParams) {
        let self = this;
        return new Promise((resolve, reject) => {

            return self.getPartnerConfig(config)
                .subscribe(config_response => {
                    let _config = config_response.json().config;
                    _config.domain = config.domain;
                    self.partner_config = _config;

                    return this.getLoyaltyConfig()
                        .subscribe(loyalty_config_response => {
                            self.inited = true;
                            self.loyalty_config = loyalty_config_response.json().config;
                            resolve(self);
                        }, reject)

                }, reject)

        });
    }

}