import { Component, OnInit, ViewEncapsulation, Directive, Input, EventEmitter, Output } from '@angular/core';
import { Response } from '@angular/http';

@Directive({
  selector: '[quizhandler]',
})
export class QuizHandler {
    @Input() currentQuest: number;
}

@Directive({
  selector: '[quizcontrols]',
  host: {
    '(click)': 'onClick($event)',
  }
})
export class QuizControls {
  @Output() clickAction = new EventEmitter<any>();

  @Input() action: string;

  constructor() {}

  onClick(event: any) {
    this.clickAction.emit(1)
  }
}