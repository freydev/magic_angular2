import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
  selector: '[container]',
  templateUrl: `./container.component.html`,
  styleUrls: [`./container.component.styl`],
})
 
export class ContainerComponent extends DefaultUIComponent {
  @Input() background: string;
  @Input('bg-modifiers') bgModifiers: string;
  @Input('bg-image') bgImage: string;
  @Input('bg-size') bgSize: string;
 }