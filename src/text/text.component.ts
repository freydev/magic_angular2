import { Component, Input, Output } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

type TextAligns = 'left' | 'right' | 'center';

@Component({
  selector: '[text]',
  styleUrls: ['./text.component.styl'],
  templateUrl: './text.component.html',
  
})

export class TextComponent extends DefaultUIComponent {
    @Input() private align: TextAligns;
}
