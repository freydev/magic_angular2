import { Component, Input } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
    selector: '[modal]',
    styleUrls: ['./modal.component.styl'],
    templateUrl: './modal.component.html'
})

export class ModalComponent extends DefaultUIComponent {

}
