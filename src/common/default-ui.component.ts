import { Input, HostBinding, Output, EventEmitter } from '@angular/core';

export abstract class DefaultUIComponent {
    @Input() modifiers: string
    @Input() theming: any = {}
    @Input() styles: any = {}
    @Input() namespace: string[];
    @Input() description: string[];
    @Input() tooltip_class: string[];

    getStyles() {
        let styles = this.styles
        
        if( this.theming ) {
            let theming = this.theming;
            styles['color'] = `${theming.color}`
            // zIndex or z-index ( O _ O )
            styles['z-index'] = `${theming['z-index']}`
            styles['cursor'] = `${theming.cursor}`
            styles['background-color'] = `${theming.bgcolor}`

            if( theming.background ) {
                styles['background-image'] = `${theming.background.image}`
                styles['background-repeat'] = `${theming.background.repeat}`
                styles['background-position'] = `${theming.background.position}`
                styles['background-size'] = `${theming.background.size}`
                // rewrite, может потом удалить, надо подумать
                styles['background-color'] = `${theming.background.color}`
            }

            if( theming.margin ) {
                styles['margin-top'] = `${theming.margin.top}px`
                styles['margin-left'] = `${theming.margin.left}px`   
                styles['margin-right'] = `${theming.margin.right}px`       
                styles['margin-bottom'] = `${theming.margin.bottom}px`       
            }

            if( theming.position ) {
                styles['top'] = `${theming.position.top}px`
                styles['left'] = `${theming.position.left}px`   
                styles['right'] = `${theming.position.right}px`       
                styles['bottom'] = `${theming.position.bottom}px`
                styles['position'] = theming.position.type || 'relative'
            }

            if( theming.font ) {
                styles['font-family'] = `${theming.font.family ? theming.font.family : ''}`
                styles['font-size'] = `${theming.font.size}px`     
                styles['font-weight'] = `${theming.font.weight}`
                styles['line-height'] = `${theming.font.height}px`
            }

            if( theming.padding ) {
                styles['padding-top'] = `${theming.padding.top}px`
                styles['padding-left'] = `${theming.padding.left}px`   
                styles['padding-right'] = `${theming.padding.right}px`       
                styles['padding-bottom'] = `${theming.padding.bottom}px`       
            }

            if( theming.size ) {
                styles['min-height'] = `${theming.size.minHeight}px`   
                styles['max-height'] = `${theming.size.maxHeight}px`
                styles['min-width'] = `${theming.size.minWidth}px`                                           
                styles['max-width'] = `${theming.size.maxWidth}px`
                styles['height'] = `${theming.size.height}px`
                styles['width'] = `${theming.size.width}px`
            }

            if( theming.borderRadius ) {
                styles['border-top-right-radius'] = `${theming.borderRadius.topRight}px`
                styles['border-top-left-radius'] = `${theming.borderRadius.topLeft}px`
                styles['border-bottom-right-radius'] = `${theming.borderRadius.bottomRight}px`
                styles['border-bottom-left-radius'] = `${theming.borderRadius.bottomLeft}px`
            }

        }

        return styles;
    }

}