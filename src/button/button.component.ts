import { Component, Input, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, Output, EventEmitter } from '@angular/core';
import { DefaultUIComponent } from '../common/default-ui.component';

@Component({
  selector: '[button]',
  styleUrls: ['./button.component.styl'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './button.component.html',
  host: {
    '(mouseover)': 'over()',
    '(mouseout)': 'leave()'
  }
})

export class ButtonComponent extends DefaultUIComponent implements OnInit {
    public current_icon: string;
    @Input() private icon: string
    @Input() private hover_icon: string
    @Input('icon-modifiers') iconModifiers: string | object
    @Input() private click: any

    ngOnInit() {
      // caching hover icon
      let cacheImg = new Image()
      cacheImg.src = this.hover_icon

      this.current_icon = this.icon;
    }


    over() {
      if( this.hover_icon )
        this.current_icon = this.hover_icon
    }

    leave() {
      this.current_icon = this.icon
    }
}
