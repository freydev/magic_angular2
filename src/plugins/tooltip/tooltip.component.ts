
import { ContentOptions,Offset } from './content/options';
import { HoveredContent } from './content/content';
import { Component,Directive,Inject, ComponentFactoryResolver, OnInit,
          AfterContentChecked,Input,Output, ElementRef, Renderer, 
          ViewContainerRef,ComponentRef, EventEmitter } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';

function getRelativeClientRect(el: any, parent: any) {
  var parentRect, rect;
  rect = el.getBoundingClientRect();
  parentRect = parent.getBoundingClientRect();
  return {
    bottom: parentRect.bottom - rect.bottom,
    height: rect.height,
    left: rect.left - parentRect.left,
    right: parentRect.right - rect.right,
    top: rect.top - parentRect.top,
    width: rect.width
  };
};

@Directive({
  selector: '[tooltip]',
  host: {
    '(mouseover)': 'onMouseHover($event)',
    '(click)': 'onClick($event)',
    '(mouseleave)' : 'onMouseLeave($event)',
    '(focus)': 'onFocus($eveent)',
    '(blur)': 'hideTooltip()'
  }
})
export class ToolTipComponent{
    @Output() beforeShow : EventEmitter<ToolTipComponent> = new EventEmitter<ToolTipComponent>();
    @Output() show : EventEmitter<ToolTipComponent> = new EventEmitter<ToolTipComponent>();
    @Output() beforeHide : EventEmitter<ToolTipComponent> = new EventEmitter<ToolTipComponent>();
    @Output() hide : EventEmitter<ToolTipComponent> = new EventEmitter<ToolTipComponent>();
    @Input()  public content: string;
    @Input()  public ngToolTipClass: string;
    @Input() tooltipDisplayOffset : Offset;
    /** set it to true, which will show tooltip on click */
    @Input() showOnClick:boolean = false;
    @Input() autoShowHide:boolean = true;
    @Input() hideOnClick:boolean = false;

    private contentCmpRef : ComponentRef<HoveredContent>;

    constructor(private _componentFactoryResolver:ComponentFactoryResolver,
                private _viewContainerRef:ViewContainerRef,
                private _renderer: Renderer,
                private _element: ElementRef,
                @Inject(DOCUMENT) private _document:any) {
    }

    private onMouseHover(event:any){
        if(!this.autoShowHide || this.showOnClick || !this.content || this._element.nativeElement.tagName == 'INPUT'){
          return;
        }

        this.buildTooltip(event);
    }
    private onClick(event:any){
      if (this.hideOnClick)
        this.hideTooltip()

      if(!this.autoShowHide || !this.showOnClick || !this.content){
          return;
        }


      if(this.hideOnClick) this.hideTooltip(); else
        this.buildTooltip(event);
    }

    private onFocus(event:any){
      if (this._element.nativeElement.tagName == 'INPUT' && this.content)
        this.buildTooltip(event);        
    }

    private onMouseLeave(event:any) {
      if (this._element.nativeElement.tagName == 'INPUT') return
        this.hideTooltip()
    }
    
    public showTooltip(options:ContentOptions){
      let componentFactory = this._componentFactoryResolver.resolveComponentFactory(HoveredContent);
      this.contentCmpRef = this._viewContainerRef.createComponent(componentFactory);
      this.beforeShow.emit(this);
      this._document.querySelector('body').appendChild(this.contentCmpRef.location.nativeElement);
      this.contentCmpRef.instance.options = options;
      this.show.emit(this);
    }
    private buildTooltip(event:any){
      let rect = getRelativeClientRect(this._element.nativeElement, document.body)
      let options:ContentOptions = {
        el_rect: rect,
        content : this.content,
        cls : this.ngToolTipClass,
        x : 0,
        y:  0,
        offset : this.tooltipDisplayOffset
      };
      this.showTooltip(options);

    }

    public hideTooltip(){
      if(this.contentCmpRef){
        this.beforeHide.emit(this);
        this.contentCmpRef.destroy();
        this.hide.emit(this);
      }
    }
}