/**
 * 
 */

export interface ContentOptions {
   content : string;
   x : number;
   y: number;
   cls : string;
   offset : Offset;
   el_rect: ClientRect
}

export interface Offset{
    x : number;
    y : number;
}