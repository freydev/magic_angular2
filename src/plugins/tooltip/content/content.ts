import { ContentOptions } from './options';

import { Component,AfterContentChecked , ElementRef, AfterViewChecked, ChangeDetectorRef } from '@angular/core';

function getOffset( elem: any ) {
    var offsetLeft = 0,
      offsetTop = 0;

    do {
      console.log('elem', elem, elem.offsetParent)
      if ( !isNaN( elem.offsetLeft ) )
      {
          offsetLeft += elem.offsetLeft;
          offsetTop += elem.offsetTop;
      }
    } while( elem = elem.offsetParent );
    return {left: offsetLeft, top: offsetTop};
}

@Component({
  template : `
            <div class="ng-tool-tip-content"
                    [ngClass]="options.cls"
                    [innerHTML] = "options.content"
                    [style.top.px]="options.y"
                    [style.left.px]="options.x">
              </div>
              `,
styles : [`
        .ng-tool-tip-content{
                z-index : 200;
                color: #333;
                background-color: #FFF;
                position: absolute;   
            }
          `]           
})

export class HoveredContent implements AfterViewChecked{

    private _options : ContentOptions;

    constructor(private elRef:ElementRef, private detectorRef: ChangeDetectorRef){
    }

    set options(op : ContentOptions){
        this._options = op;
    }

    get options():ContentOptions{
        return this._options;
    }

  ngAfterViewChecked(){
    if (this.options['checked']) return

    this.options['checked'] = true;

    let toolTip = this.elRef.nativeElement.querySelector('div.ng-tool-tip-content');
    let toolTipWidth:number = this.elRef.nativeElement.querySelector('div.ng-tool-tip-content').offsetWidth;
    let toolTipHeight:number = this.elRef.nativeElement.querySelector('div.ng-tool-tip-content').offsetHeight;

    this.options.x = this.options.el_rect.left + (this.options.el_rect.width / 2);
    this.options.y = this.options.el_rect.top;    

    if (~this.options.cls.indexOf('to-left')) {
      this.options.x = this.options.el_rect.left + this.options.el_rect.width + 25;
      this.options.y = this.options.el_rect.top + (this.options.el_rect.height / 2) + 45 + 10
    }
    else this.options.x -= toolTipWidth / 2    

    if (~this.options.cls.indexOf('to-top'))
      this.options.y += 40;
    else this.options.y -= (toolTipHeight + 20)

    this.detectorRef.detectChanges()
  }
}